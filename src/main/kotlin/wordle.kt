/**
 * @author Oscar Perona Gomez
 */

import java.util.*

    val scanner = Scanner(System.`in`)

    //DECLARACION DE VARIABLES
    var firstGame:Boolean = true
    val listOfWords = arrayOf("acido","bucal","blusa","dulce","coger","comer","nueva","salir","senda","pedir","pieza","pulso","bingo","baile","apodo","batea","corro","colmo","habia","hagan","logro","meses","peces","ratas")
    var win: Boolean= false

    //Colores
    val green_background = "\u001B[42m"
    val grey_bakcground = "\u001B[47m"
    val yellow_background = "\u001B[43m"
    val red_background = "\u001B[41m"
    val red = "\u001B[31m"
    val green = "\u001B[32m"
    val black = "\u001B[30m"
    val reset = "\u001B[0m"

/**
 * Funcion que muestrara el menu al usuario y segun la opcion que este marque le motrara las reglas o comenzara una partida
 */
fun main(){

    val word = listOfWords.random()
    win = false

    if (firstGame == true){
        do {
            println("********************")
            println("****** WORDLE ******")
            println("********************")
            repeat(2) {
                println()
            }
            println("1. Reglas")
            println("2. Jugar")
            repeat(2) {
                println()
            }
            println("Introduce una opcion del menu")
            val menu = scanner.nextInt()

            when(menu){
                1 -> {
                    println("********************")
                    println("****** REGLAS ******")
                    println("********************")
                    repeat(3) {
                        println()
                    }
                    println("- El jugador tendra que adivinar una palaba de 5 letras, para ello tendra  que ir introduciendo palabas de 5 letras has acertar la palabra ")
                    println("- El jugador tendra 6 vidas, por cada intento sin acierto perdera una vida, en caso de perder todas la vidas el jugador habra perdido la partida")
                    println("- Las letras de las palabras introducias tendran difernetes colores:")
                    println("             - $green_background$black VERDE $reset en caso de que la letra este en misma posicion que en la palabra a adivinar")
                    println("             - $yellow_background$black AMARILLO $reset en caso de que la letra esta en la palabra aconseguir pero no en la posicion correct")
                    println("             - $grey_bakcground$black GRIS $reset en caso de que la letra no este en la palabra")
                    repeat(4) {
                        println()
                    }
                }
                2 -> {
                    println("********************")
                    println("* EMPIEZA EL JUEGO *")
                    println("********************")
                    repeat(3) {
                        println()
                    }
                }
                else -> {
                    println("no es una opcion del menu")
                }
            }

        }while (menu !=2)
    }

    gameLogic(word)

    gameResult(win, word)
}

/**
 * Fuincion encargada de pedir al usuario una palabra y a traves de ella comporarla con la palabra a adivinar y gestionar el estado de la partida
 *
 * @param word:String Es la palabra que tendra que adivinar el usuario
 */
fun gameLogic(word:String){

    var lives = 6
    val repetedWords: MutableList<String> = mutableListOf("")

    println("Introduzca una palabra:")
    while (lives>0 && win == false){
        val scanner = Scanner(System.`in`)
        val imputWord = scanner.next()

        if (imputWord != word && imputWord.length == 5 && imputWord !in repetedWords){
            repetedWords.add(imputWord)
            for (i in 0..4){
                if (imputWord.get(i) == word.get(i)) print("$green_background$black ${imputWord.get(i)} $reset")
                else if (imputWord.get(i) in word) print("$yellow_background$black ${imputWord.get(i)} $reset")
                else print("$grey_bakcground$black ${imputWord.get(i)} $reset")
            }
            lives--
            print("                              Vidas restantes = $lives")
            println()
        }
        else if (imputWord == word) win = true
        else if (imputWord.length > 5) println("La palabra tiene demasiados caracteres")
        else if (imputWord.length < 5) println("La palabra tiene caracteres insuficientes")
        else if (imputWord in repetedWords) println("La palabra ya ha sido introducida")
    }

}

/**
 * Fuincion encargada de mostrar el resultado de la partida y en caso de que el usuario lo desee redirigirlo hacia una nueva partida
 *
 * @param win:Boolean Indica si el usuario ha ganado o no la partida
 * @param word:String Es la palabra que tendra que adivinar el usuario
 */
fun gameResult(win:Boolean, word:String){
    if (win == true) {
        repeat(4) {
            println()
        }
        println("$green*****************************$reset")
        println("$green*  FELICIDADES, HAS GANADO  *$reset")
        println("$green*****************************$reset")
        repeat(2) {
            println()
        }
        println("La palabra correcta era: $green_background$black $word $reset")
        println()


    }
    else {
        repeat(4) {
            println()
        }
        println("$red*******************************************$reset")
        println("$red*  Te has quedado sin vidas, has perdido  *$reset")
        println("$red*******************************************$reset")
        repeat(2) {
            println()
        }
        println("La palabra correcta era: $red_background$black $word $reset")
        println()
    }

    println("Quieres jugar de nuevo?(Y/N)")
    val newGame = scanner.next().single()

    if (newGame == 'Y' || newGame == 'y' || newGame == 's' || newGame == 'S'){
        firstGame = false
        main()
    }
    else{
        repeat(2){
            println()
        }
        println("****************************")
        println("*   GRACIAS POR JUGAR :)   *")
        println("****************************")
        repeat(2){
            println()
        }
    }
}