# wordleProyect-m03-OscarPerona


## Definicion del proyecto

Este proyecto tiene como objetivo final programar el juego *wordle*.
El juego funicionara de la siguiente manera, el usuario tendra el objetivo de adivinar una palabra de 5 letras seleccionada al azar por el programa, para ello ira introduciendo palabras hasta encontrar la correcta. Cuando el usuario introduzca una palabra esta se escribira de diferentes colores haciendo por cada letra para asi dar pistas al usuario y encaminarlo hacia la palabra correcta, los colores de las letras con los sigueintes:
- **Verde**: La letra se escribira con el fondo verde en caso de que esta este en la misma posicion que en la palabra a adivinar, es decir si el usuario introduce una palabra que empieze con 'a' y esta se pinta de color verde significa que la palabra a adivinar empieza por 'a'  
- **Amarillo**: La letra se escribira con el fondo de color amarillo en caso de que la letra si esta en la palabra a adivinar pero no en la posicion correcta
- **Gris**: La letra se escribira en un fondo gris para indicar que esta no se encuentra en la aplabra a adivinar

El usuario tendra un total de 6 vidas y por cada intento sin acierto perdera una hasta llegar a 0. El juego terminara cuando el usuario adivine la palabra lo que significara que ha ganado o por el contrario se quedara sin vidas lo que quiere decir que habra perdido la partida

## Definicion de datos

Para realizar el proyecto se utilizaran las siguientes variables:
- Un booleano para gestionar si el usuario gana o no
- Un array que contendra todas las palabras que el usuario podria llegara tener que adivinar
- Una variable para gestionar las opciones del menu
- Una variable que tendra como valor las vidas del usuario
- Una lista en las que se añadiran las palabras introducias por elususario para que asi no pueda introducir palabbras repetidas
- Varias bariables para los codigos de clores
- Una variable que utilizara el scanner para leer la palabra introducidapor el usuario

## Como ejecutar el proyecto

Para ejecutar el proyecto primero de todo se tiene que descargar el proyecto, para eso dirigite a la terminal y ejecuta el sigueinte comando

`git clone INTRODUCE AQUI EL LINK`

Una vez hecho esto se habra descargado el proyecto, el siguiente paso sera abrir IntelliJ y dentro seleccionamos la opcion open proyect para seleccionar el proyecto recien descargado.
Una vez dentro del proyecto nos dirigimos al fichero `wordle/src/main/kotlin/wordle.kt`, dentro de este fichero podremos ver el codigo y para ejecutarlo hacemos clic en el boton ejecutar en la esquina superior derecha

![Voton ejecutar](ejecutarImg.png)
